"use client";

import { useReducer, useContext } from "react";
import {
  ToastDispatchContext,
  ToastStateContext,
} from "../context/toastContext";
import { ToastProps } from "@/components/toast/Toast";

export type ToastReducerAction =
  | {
      type: "ADD_TOAST";
      toast: ToastProps;
    }
  | {
      type: "DELETE_TOAST";
      id: string;
    };

export type ToastReducerState = {
  toasts: ToastProps[];
};

function ToastReducer(state: ToastReducerState, action: ToastReducerAction) {
  switch (action.type) {
    case "ADD_TOAST": {
      return {
        ...state,
        toasts: [...state.toasts, action.toast],
      };
    }
    case "DELETE_TOAST": {
      const updatedToasts = state.toasts.filter((e) => e.id != action.id);
      return {
        ...state,
        toasts: updatedToasts,
      };
    }
    default: {
      throw new Error("unhandled action");
    }
  }
}

export function ToastProvider({ children }: React.PropsWithChildren<{}>) {
  const [state, dispatch] = useReducer(ToastReducer, {
    toasts: [],
  });

  return (
    <ToastStateContext.Provider value={state}>
      <ToastDispatchContext.Provider value={dispatch}>
        {children}
      </ToastDispatchContext.Provider>
    </ToastStateContext.Provider>
  );
}

export const useToastStateContext = () => useContext(ToastStateContext);
export const useToastDispatchContext = () => useContext(ToastDispatchContext);
