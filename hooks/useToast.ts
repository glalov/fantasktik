import { ToastType } from "@/components/toast/Toast";
import { useToastDispatchContext } from "./useToastContext";

const useToast = (delay?: number) => {
  const dispatch = useToastDispatchContext();
  const defaultDelay = 3000;

  function toast(type: ToastType, message: string) {
    const id = Math.random().toString(36).substr(2, 9);
    dispatch({
      type: "ADD_TOAST",
      toast: {
        type,
        message,
        id,
      },
    });

    setTimeout(
      () => {
        dispatch({ type: "DELETE_TOAST", id });
      },
      delay ? delay : defaultDelay
    );
  }

  return toast;
};

export default useToast;
