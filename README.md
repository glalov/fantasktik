# GifTaskTik

Task Management Application

## Description

A task management application that helps users organize and track their tasks. The application allows users to create, update, and delete tasks, set priorities and deadlines, and collaborate by assigning tasks to team members.

## Key Features

<ul>
<li>Creating, updating, and deleting tasks.</li>
<li>Assigning tasks to team members.</li>
<li>Setting task priorities and deadlines.</li>
<li>Tracking the status of tasks (e.g., in progress, completed).</li>
<li>User identification and authorization (user authentication and authorization).</li>
</ul>

## Technologies

<ul>
<li>Front-end: Using Next.js and React for the development of the client-side of the application.</li>
<li>Database: Using MongoDB to store task and user data.</li>
</ul>