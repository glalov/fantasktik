import {
  fullNameMaxLength,
  fullNameMinLength,
  passwordMaxLength,
  passwordMinLength,
} from "./formConstants";

/**
 * Checks if an email is valid.
 * @param {string} email - The email to validate.
 * @returns {boolean} - True if the email is valid, false otherwise.
 */
export const isValidEmail = (email: string) => {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
};

/**
 * Checks if a password is valid.
 * @param {string} password - The password to validate.
 * @returns {boolean} - True if the password is valid, false otherwise.
 */
export const isValidPassword = (password: string) => {
  return (
    password.length >= passwordMinLength &&
    password.length <= passwordMaxLength &&
    /\d/.test(password) &&
    /[!@#$%^&*]/.test(password)
  );
};

/**
 * Checks if a name is valid.
 * @param {string} name - The name to validate.
 * @returns {boolean} - True if the name is valid, false otherwise.
 */
export const isValidName = (name: string) => {
  return (
    name.length >= fullNameMinLength &&
    name.length <= fullNameMaxLength &&
    /^[a-zA-Z ]+$/.test(name)
  );
};
