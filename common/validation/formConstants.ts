export const fullNameMinLength = 3;
export const fullNameMaxLength = 50;
export const passwordMinLength = 6;
export const passwordMaxLength = 20;
