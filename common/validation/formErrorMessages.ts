import {
  fullNameMaxLength,
  fullNameMinLength,
  passwordMaxLength,
  passwordMinLength,
} from "./formConstants";

/**
 * Invalid email validation message.
 * @type {string}
 */
export const invalidEmail = "Please provide a valid email.";

/**
 * Invalid password validation message.
 * @type {string}
 */
export const invalidPassword = `Please provide a valid password. (${passwordMinLength}-${passwordMaxLength} characters long, at least one number and one symbol.)`;

/**
 * Invalid name validation message.
 * @type {string}
 */
export const invalidName = `Please provide a valid name. (${fullNameMinLength}-${fullNameMaxLength} characters long, only uppercase and lowercase letters.)`;
