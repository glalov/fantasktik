import { IPriority } from "@/models/priority";
import { IStatus } from "@/models/status";

export const defaultStatuses: IStatus[] = [
  {
    text: "Not Started",
    value: "1",
    order: 1,
    color: "grey",
  },
  {
    text: "Stuck",
    value: "2",
    order: 2,
    color: "red",
  },
  {
    text: "Working on it",
    value: "3",
    order: 3,
    color: "orange",
  },
  {
    text: "Done",
    value: "4",
    order: 4,
    color: "green",
  },
];

export const defaultPriorities: IPriority[] = [
  {
    text: "Default",
    value: "1",
    order: 1,
    color: "grey",
  },
  {
    text: "Low",
    value: "2",
    order: 2,
    color: "green",
  },
  {
    text: "Medium",
    value: "3",
    order: 3,
    color: "orange",
  },
  {
    text: "High",
    value: "4",
    order: 4,
    color: "red",
  },
];
