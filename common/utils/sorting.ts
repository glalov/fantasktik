export const descendingSort = (a: any, b: any, key: string) => {
  if (a[key] > b[key]) return -1;
  if (a[key] < b[key]) return 1;
  return 0;
};

export const ascendingSort = (a: any, b: any, key: string) => {
  if (a[key] > b[key]) return 1;
  if (a[key] < b[key]) return -1;
  return 0;
};
