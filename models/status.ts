export type IStatus = {
  text: string;
  value: string;
  order: number;
  color: string;
};
