import { Schema, models, model } from "mongoose";

export type ITask = {
  creatorId: Schema.Types.ObjectId;
  title: string;
  createdAt: Date;
  dueDate: Date;
  status: string;
  priority: string;
  groupId: Schema.Types.ObjectId;
  notes?: string;
  assignedTo?: Schema.Types.ObjectId;
};

export type TaskData = {
  _id: Schema.Types.ObjectId;
} & ITask;

const taskSchema = new Schema<ITask>(
  {
    creatorId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    dueDate: {
      type: Date,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    priority: {
      type: String,
      required: true,
    },
    groupId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    notes: {
      type: String,
      required: false,
    },
    assignedTo: {
      type: Schema.Types.ObjectId,
      required: false,
    },
  },
  { timestamps: true, strict: false }
);

const Task = models.Task || model<ITask>("Task", taskSchema);
export default Task;
