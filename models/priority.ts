export type IPriority = {
  text: string;
  value: string;
  order: number;
  color: string;
};
