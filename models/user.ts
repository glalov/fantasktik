import { Schema, models, model } from "mongoose";

export type IUser = {
  name: string;
  email: string;
  password: string;
  groupIds: Schema.Types.ObjectId[];
  taskIds: Schema.Types.ObjectId[];
  friendIds?: Schema.Types.ObjectId[];
  image?: string;
};

export type UserData =
  | (IUser & {
      _id: Schema.Types.ObjectId;
    })
  | null;

const userSchema = new Schema<IUser>(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    groupIds: {
      type: [Schema.Types.ObjectId],
      required: true,
    },
    taskIds: {
      type: [Schema.Types.ObjectId],
      required: true,
    },
    friendIds: {
      type: [Schema.Types.ObjectId],
      required: false,
    },
    image: {
      type: String,
      required: false,
    },
  },
  { timestamps: true, strict: false }
);

const User = models.User || model<IUser>("User", userSchema);
export default User;
