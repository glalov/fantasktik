import { Schema, models, model } from "mongoose";
import Task, { TaskData } from "./task";
import User from "./user";

export type IGroup = {
  creatorId: Schema.Types.ObjectId;
  title: string;
  createdAt: Date;
  userIds: Schema.Types.ObjectId[];
  taskIds: Schema.Types.ObjectId[];
};

export type GroupData = {
  _id: Schema.Types.ObjectId;
  tasks: TaskData[];
} & IGroup;

const groupSchema = new Schema<IGroup>(
  {
    creatorId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    userIds: {
      type: [{ type: Schema.Types.ObjectId, ref: User }],
      required: true,
    },
    taskIds: {
      type: [{ type: Schema.Types.ObjectId, ref: Task }],
      required: true,
    },
  },
  { timestamps: true, strict: false }
);

const Group = models.Group || model<IGroup>("Group", groupSchema);
export default Group;
