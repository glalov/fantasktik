import "./globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { AuthProvider } from "./Providers";
import Footer from "@/components/layout/Footer";
import NavBar from "@/components/layout/NavBar";
import { ToastProvider } from "@/hooks/useToastContext";
import ToastContainer from "@/components/toast/ToastContainer";
import SideBar from "@/components/layout/SideBar";
import Main from "@/components/layout/Main";
import FlowbiteWrapper from "@/components/layout/FlowbiteWrapper";
import ModalProvider from "@/context/modalContext";
import { CommonDataProvider } from "@/context/commonDataContext";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "FanTaskTik",
  description: "Task Management Application",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <FlowbiteWrapper>
          <ToastProvider>
            <AuthProvider>
              <CommonDataProvider>
                <ModalProvider>
                  <NavBar />
                  <SideBar />
                  <Main>{children}</Main>
                  <Footer />
                </ModalProvider>
              </CommonDataProvider>
            </AuthProvider>
            <ToastContainer />
          </ToastProvider>
        </FlowbiteWrapper>
      </body>
    </html>
  );
}
