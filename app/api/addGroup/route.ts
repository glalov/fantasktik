import { connectMongoDB } from "@/lib/mongodb";
import Group from "@/models/group";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    const newGroup = await req.json();
    await connectMongoDB();
    const createdGroup = await Group.create(newGroup);

    return NextResponse.json(createdGroup, { status: 201 });
  } catch (error) {
    return NextResponse.json(
      { message: "An error occurred while adding the group." },
      { status: 500 }
    );
  }
}
