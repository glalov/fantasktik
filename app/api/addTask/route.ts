import { connectMongoDB } from "@/lib/mongodb";
import Task from "@/models/task";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    const newTask = await req.json();
    await connectMongoDB();
    const task = await Task.create(newTask);

    return NextResponse.json(task, { status: 201 });
  } catch (error) {
    return NextResponse.json(
      { message: "An error occurred while adding the task." },
      { status: 500 }
    );
  }
}
