import { connectMongoDB } from "@/lib/mongodb";
import User from "@/models/user";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    const { groupId, userId } = await req.json();
    console.log(groupId, userId);
    await connectMongoDB();
    await User.updateOne({ _id: userId }, { $addToSet: { groupIds: groupId } });

    return NextResponse.json(
      { message: "Group added to user." },
      { status: 201 }
    );
  } catch (error) {
    return NextResponse.json(
      { message: "An error occurred while adding group to user." },
      { status: 500 }
    );
  }
}
