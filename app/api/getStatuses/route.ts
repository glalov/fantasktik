import { connectMongoDB } from "@/lib/mongodb";
import Status from "@/models/status";
import { NextResponse } from "next/server";

export async function GET() {
  try {
    await connectMongoDB();
    const statuses = await Status.find({});
    console.log("statuses: ", statuses);
    return NextResponse.json({ statuses });
  } catch (error) {
    console.log(error);
  }
}
