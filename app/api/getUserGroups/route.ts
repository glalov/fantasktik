import { connectMongoDB } from "@/lib/mongodb";
import Group from "@/models/group";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    await connectMongoDB();
    const { groupIds } = await req.json();
    const groups = await Group.find({ _id: { $in: groupIds } }).populate(
      "taskIds"
    );
    return NextResponse.json(groups);
  } catch (error) {
    console.log(error);
  }
}
