import { connectMongoDB } from "@/lib/mongodb";
import User from "@/models/user";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    const { taskId, userId } = await req.json();
    await connectMongoDB();
    const updatedUser = await User.updateOne(
      { _id: userId },
      { $addToSet: { taskIds: taskId } }
    );

    return NextResponse.json(updatedUser, { status: 201 });
  } catch (error) {
    return NextResponse.json(
      { message: "An error occurred while adding task to user." },
      { status: 500 }
    );
  }
}
