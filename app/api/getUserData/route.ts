import { connectMongoDB } from "@/lib/mongodb";
import User from "@/models/user";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    await connectMongoDB();
    const { id: _id } = await req.json();
    const user = await User.findOne({ _id }).select("-password");
    return NextResponse.json(user);
  } catch (error) {
    console.log(error);
  }
}
