import { connectMongoDB } from "@/lib/mongodb";
import Group from "@/models/group";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    const { taskId, groupId } = await req.json();
    await connectMongoDB();
    const updatedGroup = await Group.updateOne(
      { _id: groupId },
      { $addToSet: { taskIds: taskId } }
    );

    return NextResponse.json(updatedGroup, { status: 201 });
  } catch (error) {
    return NextResponse.json(
      { message: "An error occurred while adding task to group." },
      { status: 500 }
    );
  }
}
