import UserInfo from "@/components/UserInfo";
import DashboardPage from "@/pages/DashboardPage";

export default function Dashboard() {
  return <DashboardPage />;
}
