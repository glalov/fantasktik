import { IStatus } from "@/models/status";

export const getAllStatuses: () => Promise<IStatus[]> = async () => {
  const res = await fetch("api/getStatuses", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  return await res.json();
};
