import { UserData } from "@/models/user";
import { Schema } from "mongoose";

export const getUserData: (
  id: Schema.Types.ObjectId | undefined
) => Promise<UserData | null> = async (
  id: Schema.Types.ObjectId | undefined
) => {
  const res = await fetch("/api/getUserData", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ id }),
  });

  if (!res.ok) {
    return null;
  }

  return await res.json();
};

export const addTaskToUser = async (
  taskId: Schema.Types.ObjectId,
  userId: Schema.Types.ObjectId
) => {
  const res = await fetch("/api/addTaskToUser", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ taskId, userId }),
  });

  return res;
};

export const addGroupToUser = async (
  groupId: Schema.Types.ObjectId,
  userId: Schema.Types.ObjectId
) => {
  const res = await fetch("/api/addGroupToUser", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ groupId, userId }),
  });

  return res;
};
