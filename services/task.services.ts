import { ITask, TaskData } from "@/models/task";
import { addTaskToGroup } from "./group.services";
import { addTaskToUser } from "./user.services";

export const addTask = async (task: ITask) => {
  let res = await fetch("api/addTask", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(task),
  });

  if (!res.ok) return res;

  const newTask = (await res.json()) as TaskData;

  res = await addTaskToGroup(newTask._id, task.groupId);

  if (!res.ok) return res;

  res = await addTaskToUser(newTask._id, task.creatorId);

  return res;
};
