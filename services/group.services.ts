import { GroupData, IGroup } from "@/models/group";
import { Schema } from "mongoose";

export const addGroup = async (group: IGroup) => {
  const res = await fetch("api/addGroup", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(group),
  });
  return res;
};

export const getUserGroups: (
  groupIds: Schema.Types.ObjectId[]
) => Promise<GroupData[]> = async (groupIds: Schema.Types.ObjectId[]) => {
  const res = await fetch("api/getUserGroups", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ groupIds }),
  });

  if (!res.ok) {
    return [];
  }

  return await res.json();
};

export const addTaskToGroup = async (
  taskId: Schema.Types.ObjectId,
  groupId: Schema.Types.ObjectId
) => {
  const res = await fetch("api/addTaskToGroup", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ taskId, groupId }),
  });
  return res;
};
