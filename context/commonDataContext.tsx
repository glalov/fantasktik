"use client";

import { defaultPriorities, defaultStatuses } from "@/common/defaults";
import { GroupData } from "@/models/group";
import { IPriority } from "@/models/priority";
import { IStatus } from "@/models/status";
import { UserData } from "@/models/user";
import { getUserGroups } from "@/services/group.services";
import { getAllStatuses } from "@/services/status.services";
import { getUserData } from "@/services/user.services";
import { Schema } from "mongoose";
import { useSession } from "next-auth/react";
import { createContext, useState, useEffect } from "react";

export type CommonData = {
  userData: UserData;
  setUserData: (userData: UserData) => void;
  groupsData: GroupData[];
  statuses: IStatus[];
  priorities: IPriority[];
};

export const CommonDataContext = createContext<CommonData>({
  userData: null,
  setUserData: () => {},
  groupsData: [],
  statuses: defaultStatuses,
  priorities: defaultPriorities,
});

export const CommonDataProvider = ({
  children,
}: React.PropsWithChildren<{}>) => {
  const [statuses, setStatuses] = useState<IStatus[]>(defaultStatuses);
  const [priorities, setPriorities] = useState<IPriority[]>(defaultPriorities);
  const [userData, setUser] = useState<UserData>(null);
  const { data: session } = useSession();
  const [groupsData, setGroups] = useState<GroupData[]>([]);

  useEffect(() => {
    const fillUserData = async () => {
      const retUserData = await getUserData(session?.user?.id);
      setUser(retUserData);
      if (retUserData?.groupIds) await fillGroups(retUserData?.groupIds);
    };

    const fillGroups = async (groupIds: Schema.Types.ObjectId[]) => {
      const retGroups = await getUserGroups(groupIds);
      setGroups(retGroups);
    };

    fillUserData();
  }, [session?.user?.id]);

  return (
    <CommonDataContext.Provider
      value={{
        userData,
        setUserData: setUser,
        groupsData,
        statuses,
        priorities,
      }}
    >
      {children}
    </CommonDataContext.Provider>
  );
};
