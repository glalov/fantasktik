import { ToastReducerAction, ToastReducerState } from "@/hooks/useToastContext";
import { Dispatch, createContext } from "react";

export const ToastStateContext = createContext<ToastReducerState>({
  toasts: [],
});
export const ToastDispatchContext = createContext<Dispatch<ToastReducerAction>>(
  () => {}
);
