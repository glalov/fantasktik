"use client";

import UnifiedModal from "@/components/modal/UnifiedModal";
import { createContext, useState } from "react";

export type ModalState = {
  openModal: (content: React.ReactNode) => void;
  closeModal: () => void;
};

export const ModalStateContext = createContext<ModalState>({
  openModal: () => {
    throw new Error("openModal function must be overridden");
  },
  closeModal: () => {
    throw new Error("closeModal function must be overridden");
  },
});

const ModalProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalContent, setModalContent] = useState<React.ReactNode>(null);

  const openModal = (content: React.ReactNode) => {
    setIsModalOpen(true);
    setModalContent(content);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setModalContent(null);
  };

  return (
    <ModalStateContext.Provider
      value={{
        openModal,
        closeModal,
      }}
    >
      {children}
      <UnifiedModal
        isOpen={isModalOpen}
        onClose={closeModal}
        content={modalContent}
      />
    </ModalStateContext.Provider>
  );
};

export default ModalProvider;
