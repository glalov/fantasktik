import AddGroup from "@/components/group/AddGroup";
import React from "react";

const DashboardPage = () => {
  return (
    <div>
      <div>Dashboard Page</div>
      <AddGroup />
    </div>
  );
};

export default DashboardPage;
