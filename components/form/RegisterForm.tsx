"use client";

import Link from "next/link";
import { useState } from "react";
import { useRouter } from "next/navigation";
import { Button, Checkbox, Label, TextInput, Alert } from "flowbite-react";
import { HiInformationCircle } from "react-icons/hi";
import {
  isValidEmail,
  isValidName,
  isValidPassword,
} from "@/common/validation/formValidations";
import {
  invalidEmail,
  invalidName,
  invalidPassword,
} from "@/common/validation/formErrorMessages";
import { signIn } from "next-auth/react";
import useToast from "@/hooks/useToast";

type RegisterForm = {
  name: string;
  email: string;
  password: string;
};

export default function RegisterForm() {
  const [form, setForm] = useState<RegisterForm>({
    name: "",
    email: "",
    password: "",
  });
  const [validated, setValidated] = useState(false);
  const [error, setError] = useState("");

  const toast = useToast();

  const updateForm = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    setValidated(true);

    if (!form.name || !form.email || !form.password) {
      setError("All fields are necessary.");
      return;
    }

    if (
      !isValidEmail(form.email) ||
      !isValidName(form.name) ||
      !isValidPassword(form.password)
    ) {
      return;
    }

    try {
      const resUserExists = await fetch("api/userExists", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email: form.email }),
      });

      const { user } = await resUserExists.json();

      if (user) {
        setError("User already exists.");
        return;
      }

      const res = await fetch("api/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(form),
      });

      if (res.ok) {
        const res = await signIn("credentials", {
          email: form.email,
          password: form.password,
          redirect: false,
        });

        if (res?.error) {
          setError("Invalid Credentials. Please try again.");
          return;
        }

        router.replace("dashboard");
        toast("success", "Register successful");
      } else {
        setError("User registration failed. Please try again.");
      }
    } catch (error) {
      setError("Error during registration: " + error);
    }
  };

  return (
    <div className="bg-gray-100 flex justify-center items-center h-screen">
      <div className="w-1/2 h-screen hidden lg:block">
        <img
          src="https://placehold.co/800x/667fff/ffffff.png?text=Your+Image&font=Montserrat"
          alt="Login Image"
          className="object-cover w-full h-full"
        />
      </div>

      <div className="lg:p-36 md:p-52 sm:20 p-8 w-full lg:w-1/2">
        <h1 className="text-2xl font-semibold mb-4">Register</h1>
        <form onSubmit={handleSubmit} className="flex max-w-md flex-col gap-4">
          <div>
            <div className="mb-2 block">
              <Label
                color={
                  validated && !isValidName(form.name) ? "failure" : undefined
                }
                htmlFor="name1"
                value="Your full name"
              />
            </div>
            <TextInput
              color={
                validated && !isValidName(form.name) ? "failure" : undefined
              }
              helperText={
                validated && !isValidName(form.name) ? invalidName : null
              }
              id="name1"
              required
              type="text"
              name="name"
              onChange={(e) => updateForm(e)}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label
                color={
                  validated && !isValidEmail(form.email) ? "failure" : undefined
                }
                htmlFor="email1"
                value="Your email"
              />
            </div>
            <TextInput
              color={
                validated && !isValidEmail(form.email) ? "failure" : undefined
              }
              helperText={
                validated && !isValidEmail(form.email) ? invalidEmail : null
              }
              id="email1"
              placeholder="name@email.com"
              required
              type="email"
              name="email"
              onChange={(e) => updateForm(e)}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label
                color={
                  validated && !isValidPassword(form.password)
                    ? "failure"
                    : "primary"
                }
                htmlFor="password1"
                value="Your password"
              />
            </div>
            <TextInput
              color={
                validated && !isValidPassword(form.password)
                  ? "failure"
                  : undefined
              }
              helperText={
                validated && !isValidPassword(form.password)
                  ? invalidPassword
                  : null
              }
              id="password1"
              required
              type="password"
              name="password"
              onChange={(e) => updateForm(e)}
            />
          </div>
          <Button type="submit">Register</Button>
          {error && (
            <Alert color="failure" icon={HiInformationCircle}>
              <span>
                <p>
                  <span className="font-medium">{error}</span>
                </p>
              </span>
            </Alert>
          )}

          <Link className="text-sm mt-3 text-right" href={"/login"}>
            Already have an account? <span className="underline">Login</span>
          </Link>
        </form>
      </div>
    </div>
  );
}
