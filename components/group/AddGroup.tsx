"use client";

import { CommonDataContext } from "@/context/commonDataContext";
import useToast from "@/hooks/useToast";
import { GroupData, IGroup } from "@/models/group";
import { Alert, TextInput } from "flowbite-react";
import React, { useContext, useState } from "react";
import Button from "../button/Button";
import { HiInformationCircle } from "react-icons/hi";
import { addGroup } from "@/services/group.services";
import { addGroupToUser } from "@/services/user.services";
import { Schema } from "mongoose";

const AddGroup = () => {
  const { userData } = useContext(CommonDataContext);
  const defaultForm: IGroup = {
    creatorId: userData?._id || ("" as unknown as Schema.Types.ObjectId),
    title: "",
    createdAt: new Date(),
    userIds: userData?._id ? [userData?._id] : [],
    taskIds: [],
  };
  const [form, setForm] = useState<IGroup>(defaultForm);
  const [error, setError] = useState("");
  const toast = useToast();

  const updateForm = (
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >
  ) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const onAdd = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      const res = await addGroup(form);

      if (res.ok) {
        const newGroupData = (await res.json()) as GroupData;
        const addGroupToUserRes = await addGroupToUser(
          newGroupData._id,
          userData?._id || ("" as unknown as Schema.Types.ObjectId)
        );
        if (addGroupToUserRes.ok) {
          toast("success", "Group Added");
          setForm(defaultForm);
          setError("");
        } else {
          setError("Error during adding group to user. Please try again.");
        }
      } else {
        setError("Error during adding. Please try again.");
      }
    } catch (err) {
      setError("Error during adding: " + error);
    }
  };
  return (
    <div>
      <form onSubmit={onAdd}>
        <TextInput
          id="title"
          name="title"
          placeholder="Group name"
          required
          value={form.title}
          onChange={(e) => updateForm(e)}
        />
        <Button type="submit" color="success">
          Add
        </Button>
      </form>
      {error && (
        <Alert color="failure" icon={HiInformationCircle}>
          <span>
            <p>
              <span className="font-medium">{error}</span>
            </p>
          </span>
        </Alert>
      )}
    </div>
  );
};

export default AddGroup;
