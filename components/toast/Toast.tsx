"use client";

import { useToastDispatchContext } from "@/hooks/useToastContext";
import { Toast as FlowbiteToast } from "flowbite-react";
import { HiCheck, HiX } from "react-icons/hi";

export type ToastType = "success" | "delete";

export type ToastProps = {
  id: string;
  message: string;
  type: ToastType;
};

const getTypeClassName = (type: ToastType) => {
  switch (type) {
    case "success":
      return "bg-green-100 text-green-500 dark:bg-green-800 dark:text-green-200";
    case "delete":
      return "bg-red-100 text-red-500 dark:bg-red-800 dark:text-red-200";
    default:
      return null;
  }
};

const getIcon = (type: ToastType) => {
  switch (type) {
    case "success":
      return <HiCheck className="h-5 w-5" />;
    case "delete":
      return <HiX className="h-5 w-5" />;
    default:
      return null;
  }
};

const Toast = ({ id, message, type }: ToastProps) => {
  const dispatch = useToastDispatchContext();
  return (
    <FlowbiteToast className="fixed top-5 right-5">
      <div
        key={id}
        className={
          "inline-flex h-8 w-8 shrink-0 items-center justify-center rounded-lg " +
          getTypeClassName(type)
        }
      >
        {getIcon(type)}
      </div>
      <div className="ml-3 text-sm font-normal">{message}</div>
      <FlowbiteToast.Toggle
        onClick={() => {
          dispatch({ type: "DELETE_TOAST", id });
        }}
      />
    </FlowbiteToast>
  );
};

export default Toast;
