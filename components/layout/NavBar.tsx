"use client";

import { Navbar } from "flowbite-react";
import UserMenu from "./UserMenu";
import { useRouter } from "next/navigation";
import { useSession } from "next-auth/react";
import LoginButton from "../button/LoginButton";
import RegisterButton from "../button/RegisterButton";
import { DarkThemeToggle } from "flowbite-react";

const NavBar = () => {
  const router = useRouter();
  const { status } = useSession();

  return (
    <Navbar fluid rounded className="fixed top-0 w-full h-16">
      <Navbar.Brand onClick={() => router.push("/")} className="cursor-pointer">
        <img
          alt="FanTaskTik Logo"
          className="mr-3 h-6 sm:h-9"
          src="/favicon.svg"
        />
        <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
          FanTaskTik
        </span>
      </Navbar.Brand>
      <DarkThemeToggle />
      {status === "authenticated" && <UserMenu />}
      {status !== "authenticated" ? (
        <div>
          <LoginButton />
          <RegisterButton />
        </div>
      ) : null}
    </Navbar>
  );
};

export default NavBar;
