"use client";

import { useSession } from "next-auth/react";
import { twMerge } from "tailwind-merge";

const Main = ({ children }: { children: React.ReactNode }) => {
  const { status } = useSession();
  return (
    <main
      className={twMerge(
        "mt-16 min-h-screen",
        status === "authenticated" && "ml-16 lg:ml-64"
      )}
    >
      {children}
    </main>
  );
};

export default Main;
