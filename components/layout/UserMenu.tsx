"use client";

import { Avatar, Dropdown, Navbar } from "flowbite-react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { signOut } from "next-auth/react";

const UserMenu = () => {
  const { data: session } = useSession();
  const router = useRouter();
  const OnSignOut = () => {
    signOut();
    router.replace("/");
  };
  return (
    <div className="flex md:order-2">
      <Dropdown
        arrowIcon={false}
        inline
        label={
          <Avatar
            alt="User settings"
            img="https://flowbite.com/docs/images/people/profile-picture-5.jpg"
            rounded
          />
        }
      >
        <Dropdown.Header>
          <span className="block text-sm">{session?.user?.name}</span>
          <span className="block truncate text-sm font-medium">
            {session?.user?.email}
          </span>
        </Dropdown.Header>
        <Dropdown.Item>Profile</Dropdown.Item>
        <Dropdown.Item>Settings</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item onClick={OnSignOut}>Sign out</Dropdown.Item>
      </Dropdown>
    </div>
  );
};

export default UserMenu;
