"use client";

import { Flowbite } from "flowbite-react";

const FlowbiteWrapper = ({ children }: React.PropsWithChildren) => {
  return <Flowbite>{children}</Flowbite>;
};

export default FlowbiteWrapper;
