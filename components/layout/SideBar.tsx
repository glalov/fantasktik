"use client";

import { Sidebar } from "flowbite-react";
import { useContext, useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import {
  HiArrowSmRight,
  HiChartPie,
  HiInbox,
  HiShoppingBag,
  HiTable,
  HiUser,
  HiViewBoards,
} from "react-icons/hi";
import { MdAddTask } from "react-icons/md";
import Link from "next/link";
import { ModalStateContext } from "@/context/modalContext";
import AddTaskModal from "@/components/modal/AddTaskModal";

const SideBar = () => {
  const router = useRouter();
  const { status } = useSession();
  const { openModal } = useContext(ModalStateContext);

  // Collapse Sidebar on mobile
  const [collapsed, setCollapsed] = useState(false);
  useEffect(() => {
    const handleResize = () => {
      const viewportWidth = window.innerWidth;
      if (viewportWidth < 1024) {
        setCollapsed(true);
      } else {
        setCollapsed(false);
      }
    };

    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const routeTo = (path: string) => {
    router.push(path);
  };

  // If user is not authenticated, don't show sidebar
  if (status !== "authenticated") {
    return null;
  }

  return (
    <Sidebar
      className="fixed h-full left-0 top-16 w-16 lg:w-64"
      collapsed={collapsed}
    >
      <Sidebar.Items>
        <Sidebar.ItemGroup>
          <Sidebar.Item
            className="cursor-pointer"
            onClick={() => openModal(<AddTaskModal />)}
            icon={MdAddTask}
          >
            <p>Add Task</p>
          </Sidebar.Item>
        </Sidebar.ItemGroup>
        <Sidebar.ItemGroup>
          <Sidebar.Item
            className="cursor-pointer"
            onClick={() => routeTo("/dashboard")}
            icon={HiChartPie}
          >
            <p>Dashboard</p>
          </Sidebar.Item>
        </Sidebar.ItemGroup>
        {/* <Sidebar.Item href="#" icon={HiViewBoards}>
            <p>Kanban</p>
          </Sidebar.Item>
          <Sidebar.Item href="#" icon={HiInbox}>
            <p>Inbox</p>
          </Sidebar.Item>
          <Sidebar.Item href="#" icon={HiUser}>
            <p>Users</p>
          </Sidebar.Item>
          <Sidebar.Item href="#" icon={HiShoppingBag}>
            <p>Products</p>
          </Sidebar.Item>
          <Sidebar.Item href="#" icon={HiArrowSmRight}>
            <p>Sign In</p>
          </Sidebar.Item>
          <Sidebar.Item href="#" icon={HiTable}>
            <p>Sign Up</p>
          </Sidebar.Item>
        </Sidebar.ItemGroup>
        <Sidebar.ItemGroup>
          <Sidebar.Item href="#" icon={HiChartPie}>
            <p>Upgrade to Pro</p>
          </Sidebar.Item>
          <Sidebar.Item href="#" icon={HiViewBoards}>
            <p>Documentation</p>
          </Sidebar.Item>
        </Sidebar.ItemGroup> */}
      </Sidebar.Items>
    </Sidebar>
  );
};

export default SideBar;
