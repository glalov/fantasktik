"use client";

import React, { useContext } from "react";
import Button from "./Button";
import { useRouter } from "next/navigation";
import { signOut } from "next-auth/react";
import { CommonDataContext } from "@/context/commonDataContext";

const SignOutButton = () => {
  const router = useRouter();
  const { setUserData } = useContext(CommonDataContext);
  const OnSignOut = () => {
    signOut();
    setUserData(null);
    router.replace("/");
  };
  return <Button onClick={OnSignOut}>Sign Out</Button>;
};

export default SignOutButton;
