"use client";

import React from "react";
import Button from "./Button";
import { useRouter } from "next/navigation";

const LoginButton = () => {
  const router = useRouter();
  const LogInRedirect = () => {
    router.push("/login");
  };
  return <Button onClick={LogInRedirect}>Log In</Button>;
};

export default LoginButton;
