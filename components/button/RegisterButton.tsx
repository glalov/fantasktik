"use client";

import React from "react";
import Button from "./Button";
import { useRouter } from "next/navigation";

const RegisterButton = () => {
  const router = useRouter();
  const RegisterRedirect = () => {
    router.push("/register");
  };
  return <Button onClick={RegisterRedirect}>Register</Button>;
};

export default RegisterButton;
