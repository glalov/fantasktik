"use client";

import React from "react";
import { Button as FlowbiteButton } from "flowbite-react";

type ButtonProps = {
  children: React.ReactNode;
  onClick?: () => void;
  type?: "button" | "submit" | "reset";
  color?:
    | "blue"
    | "gray"
    | "dark"
    | "light"
    | "success"
    | "failure"
    | "warning"
    | "purple";
};

const Button = ({ children, onClick, type, color }: ButtonProps) => {
  return (
    <FlowbiteButton
      onClick={() => {
        if (onClick) onClick();
      }}
      type={type || "button"}
      color={color}
    >
      {children}
    </FlowbiteButton>
  );
};

export default Button;
