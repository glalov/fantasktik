import React from "react";

import { Modal } from "flowbite-react";

type UnifiedModalProps = {
  isOpen: boolean;
  onClose: () => void;
  content: React.ReactNode;
};

const UnifiedModal = ({ isOpen, onClose, content }: UnifiedModalProps) => {
  return (
    <Modal show={isOpen} size="md" popup onClose={() => onClose()}>
      {content}
    </Modal>
  );
};

export default UnifiedModal;
