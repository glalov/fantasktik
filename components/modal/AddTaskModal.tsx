"use client";

import { useContext, useState, useEffect } from "react";

import {
  Alert,
  Label,
  Modal,
  TextInput,
  ToggleSwitch,
  Select,
  Textarea,
  Datepicker,
} from "flowbite-react";
import Button from "../button/Button";
import { HiInformationCircle } from "react-icons/hi";
import useToast from "@/hooks/useToast";
import { ModalStateContext } from "@/context/modalContext";
import { ITask } from "@/models/task";
import { IStatus } from "@/models/status";
import { addTask } from "@/services/task.services";
import { CommonDataContext } from "@/context/commonDataContext";
import { ascendingSort } from "@/common/utils/sorting";
import { IPriority } from "@/models/priority";
import { Schema } from "mongoose";

const AddTaskModal = () => {
  const [additionalElementSwitch, setAdditionalElementSwitch] = useState(false);
  const [error, setError] = useState("");
  const { closeModal } = useContext(ModalStateContext);
  const { userData, groupsData, statuses, priorities } =
    useContext(CommonDataContext);
  const toast = useToast();
  const defaultForm: ITask = {
    creatorId: userData?._id || ("" as unknown as Schema.Types.ObjectId),
    title: "",
    createdAt: new Date(),
    dueDate: new Date(),
    status: statuses.length ? statuses[0].value : "1",
    priority: priorities.length ? priorities[0].value : "1",
    groupId: userData?.groupIds[0] || ("" as unknown as Schema.Types.ObjectId),
    notes: "",
    //assignedTo: "" as unknown as Schema.Types.ObjectId,
  };
  const [form, setForm] = useState<ITask>(defaultForm);

  console.log("groupsData", groupsData);

  const updateForm = (
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >
  ) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  const clearAdditionalFormElements = () => {
    setForm({
      ...defaultForm,
      title: form.title,
    });
  };
  const onAdditionalElementSwitchChange = (checked: boolean) => {
    setAdditionalElementSwitch(checked);
    if (!checked) {
      clearAdditionalFormElements();
    }
  };

  const onAdd = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      const res = await addTask(form);

      if (res.ok) {
        toast("success", "Task Added");
        closeModal();
      } else {
        setError("Error during adding. Please try again.");
      }
    } catch (err) {
      setError("Error during adding: " + error);
    }
  };

  return (
    <>
      <Modal.Header>Add Task</Modal.Header>
      <form onSubmit={onAdd}>
        <Modal.Body>
          <TextInput
            id="title"
            name="title"
            placeholder="Task"
            required
            onChange={(e) => updateForm(e)}
          />
          <ToggleSwitch
            checked={additionalElementSwitch}
            label="More Options"
            onChange={onAdditionalElementSwitchChange}
          />
          {additionalElementSwitch && (
            <>
              <Label htmlFor="dueDate" value="Due Date" />
              <Datepicker
                id="dueDate"
                weekStart={2}
                minDate={new Date()}
                onSelectedDateChanged={(date: Date) =>
                  setForm({
                    ...form,
                    dueDate: date,
                  })
                }
              />
              <Label htmlFor="status" value="Status" />
              <Select id="status" name="status" onChange={(e) => updateForm(e)}>
                {statuses
                  .sort((a, b) => ascendingSort(a, b, "order"))
                  .map((status: IStatus) => (
                    <option key={status.value} value={status.value}>
                      {status.text}
                    </option>
                  ))}
              </Select>
              <Label htmlFor="priority" value="Priority" />
              <Select
                id="priority"
                name="priority"
                onChange={(e) => updateForm(e)}
              >
                {priorities
                  .sort((a, b) => ascendingSort(a, b, "order"))
                  .map((priority: IPriority) => (
                    <option key={priority.value} value={priority.value}>
                      {priority.text}
                    </option>
                  ))}
              </Select>
              <Label htmlFor="group" value="Group" />
              {groupsData.length && (
                <Select id="group" name="group" onChange={(e) => updateForm(e)}>
                  {groupsData.map((group) => (
                    <option
                      key={group._id as unknown as string}
                      value={group._id as unknown as string}
                    >
                      {group.title}
                    </option>
                  ))}
                </Select>
              )}
              <Textarea
                id="notes"
                name="notes"
                placeholder="Notes"
                rows={3}
                onChange={(e) => updateForm(e)}
              />
            </>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit" color="success">
            Add
          </Button>
        </Modal.Footer>
      </form>
      {error && (
        <Alert color="failure" icon={HiInformationCircle}>
          <span>
            <p>
              <span className="font-medium">{error}</span>
            </p>
          </span>
        </Alert>
      )}
    </>
  );
};

export default AddTaskModal;
